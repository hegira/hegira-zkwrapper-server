/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper.dlm;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessMultiLock;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LockManager implements ILockManager{
	private static String connectString;
	private static ConcurrentHashMap<String, CuratorFramework> clientsMap;
	
	private static LockManager instance = null;
	private static Object instance_lock = new Object();
	
	private String dlm_basepath = "/hegira/dlm";
	private String dataMigrationId;
	private static Logger log = LoggerFactory.getLogger(LockManager.class);
	
	private LockManager(String connectString, String dataMigrationId) {
		LockManager.connectString = connectString;
		clientsMap = new ConcurrentHashMap<String, CuratorFramework>(1);
		this.dataMigrationId = dataMigrationId;
		@SuppressWarnings("resource")
		CuratorFramework client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		if(dataMigrationId != null && client!=null){
			clientsMap.put(dataMigrationId, client);
			connect(dataMigrationId);
		}
	}
	
	/**
	 * Creates a new, or retrieves an already existing, {@link LockManager} instance.
	 * When creating a new instance, a connection to ZooKeeper (one for each dataMigrationId) is established and it is not
	 * released until disconnect() is explicitly called on the instance.
	 * @param cs ZooKeeper connect string (host:port)
	 * @param dataMigrationId The id of the data migration
	 * @return The instance.
	 */
	public static LockManager getInstance(String cs, String dataMigrationId){
		if(cs==null || dataMigrationId == null)
			throw new IllegalArgumentException("Zookeeper connect string can't be null");
		synchronized(instance_lock){
			if(instance == null && connectString==null){
				instance = new LockManager(cs, dataMigrationId);
			}else if(!cs.equals(connectString)){
				//connection string has changed
				disconnect_static(dataMigrationId);
				instance = new LockManager(cs, dataMigrationId);
			}
			//if it was manually disconnected, it should reconnect
			if(!isConnected(dataMigrationId))
				connect(dataMigrationId);
		}
		return instance;
	}
	
	/**
	 * Checks whether the instance is connected to ZooKeeper.
	 * @param dataMigrationId The data migration id.
	 * @return The result
	 */
	public static boolean isConnected(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		return clientsMap!= null && clientsMap.get(dataMigrationId) != null && 
				clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED) ? 
						true : false;
	}
	
	private static void connect(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		if(isConnected(dataMigrationId)){
			log.warn("{} - Already connected.",
					Thread.currentThread().getName());
			return;
		}
		while(!clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED))
			clientsMap.get(dataMigrationId).start();
		log.debug("{} - Connected.",
					Thread.currentThread().getName());
	}
	
	/**
	 * Releases the connection for the given data migration id.
	 * @param dataMigrationId The migration id.
	 */
	public void disconnect(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		disconnect_static(dataMigrationId);
	}
	
	private static void disconnect_static(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		if(!isConnected(dataMigrationId)) return;
		while(clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED))
			clientsMap.get(dataMigrationId).close();
		CuratorFramework removedCF = clientsMap.remove(dataMigrationId);
		removedCF.close();
		log.debug("{} - Disconnected.",
					Thread.currentThread().getName());
	}
	
	private String getPath(String tableName, Integer sequentialId){
		if(tableName==null || sequentialId<0)
			throw new IllegalArgumentException("Wrong parameters");
		return dlm_basepath+"/"+dataMigrationId+"/"+tableName+"/"+sequentialId;
	}
	
	/**
	 * Tries to acquire an exclusive, reentrant lock on the given sequentialId element, for the time specified by the 
	 * parameters time and unit.
	 * @param tableName The name of the table containing the sequentialId element.
	 * @param sequentialId The sequentialId characterizing the element.
	 * @param time The amount of time to wait for acquiring the exclusive lock.
	 * @param unit The measurement unit for the time parameter.
	 * @return If acquired returns the mutex, otherwise, if an error occurs, it returns <b>null</b>
	 * @throws IllegalStateException If a lock cannot be obtained within the given amount of time, this exception is thrown.
	 */
	public InterProcessMutex acquireLock(String tableName, Integer sequentialId, long time, TimeUnit unit)
		throws IllegalStateException {
		if(tableName==null || sequentialId<0 || time <= 0 || unit==null)
			throw new IllegalArgumentException("Wrong parameters");
		if(!isConnected(dataMigrationId)){
			log.error("{} - Not connected",
					Thread.currentThread().getName());
			return null;
		}
		
		InterProcessMutex mutex = new InterProcessMutex(clientsMap.get(dataMigrationId), getPath(tableName, sequentialId));
		try {
			if (!mutex.acquire(time, unit)){
				throw new IllegalStateException(Thread.currentThread().getName() + " could not acquire the lock");
			}
			return mutex;
		} catch (IllegalStateException e) {
			throw e;
		} catch (Exception e) {
			log.error("{} - Error acquiring the lock for resource {}/{}",
					Thread.currentThread().getName(),
					tableName, sequentialId, e);
		}
		return mutex=null;
	}
	
	/**
	 * Tries to atomically acquire locks for the provided resources. Either all or none of them are acquired.
	 * @param txId The id of the transaction
	 * @param tbls_Ids A Map having as Key the table name and as value the resource Id (sequentialId) to lock
	 * @param time The amount of time to wait for acquiring the exclusive lock.
	 * @param unit The measurement unit for the time parameter.
	 * @return If acquired returns the mutex, otherwise, if an error occurs, it returns <b>null</b>
	 * @throws IllegalStateException If a lock cannot be obtained within the given amount of time, this exception is thrown.
	 */
	public InterProcessMultiLock acquireLocks(String txId, Map<String, SimpleEntry<String,Integer>> tbls_Ids, long time, TimeUnit unit)
			throws IllegalStateException {
		if(tbls_Ids==null || time <= 0 || unit==null)
			throw new IllegalArgumentException("Wrong parameters");
		if(!isConnected(dataMigrationId)){
			log.error("{} - Not connected",
					Thread.currentThread().getName());
			return null;
		}
		
		List<InterProcessLock> mutexList = new ArrayList<InterProcessLock>(tbls_Ids.size());
		
		Iterator<Entry<String, SimpleEntry<String, Integer>>> iterator = tbls_Ids.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<String, SimpleEntry<String, Integer>> mapEntry = iterator.next();
			SimpleEntry<String, Integer> mapValue = mapEntry.getValue();
			InterProcessMutex mutex = 
					new InterProcessMutex(clientsMap.get(dataMigrationId), getPath(mapValue.getKey(), mapValue.getValue()));
			mutexList.add(mutex);
		}
		
		InterProcessMultiLock multiLock = new InterProcessMultiLock(mutexList);
		try {
			if (!multiLock.acquire(time, unit)){
				throw new IllegalStateException(Thread.currentThread().getName() + " could not acquire the lock");
			}
			return multiLock;
		} catch (IllegalStateException e) {
			throw e;
		} catch (Exception e) {
			log.error("{} - Error acquiring the lock for tx {}. #Resources {}",
					Thread.currentThread().getName(),
					txId, tbls_Ids.size(), e);
		}
		return multiLock=null;
	}
	
	/**
	 * Releases the lock on the given mutex.
	 * @param mutex The mutex obtained calling the acquireLock method.
	 * @return <b>true</b> if it succeeds, <b>false</b> otherwise. 
	 */
	public boolean releaseLock(InterProcessMutex mutex){
		if(mutex==null){
			throw new IllegalArgumentException("Wrong parameter");
		}
		try {
			mutex.release();
			mutex=null;
			return true;
		} catch (Exception e) {
			log.error("{} - Error releasing the lock",
					Thread.currentThread().getName(), e);
		}
		return false;
	}
	
	
	/**
	 * Releases the locks on the given multilock.
	 * @param multilock The multilock obtained calling the acquireLocks method.
	 * @return <b>true</b> if it succeeds, <b>false</b> otherwise. 
	 */
	public boolean releaseLocks(InterProcessMultiLock multilock){
		if(multilock==null){
			throw new IllegalArgumentException("Wrong parameter");
		}
		try {
			multilock.release();
			multilock=null;
			return true;
		} catch (Exception e) {
			log.error("{} - Error releasing the multilock",
					Thread.currentThread().getName(), e);
		}
		return false;
	}
	
	/**
	 * Releases either an {@link InterProcessMutex} or an {@link InterProcessMultiLock}.
	 * @param lock The lock to release obtained by calling either acquireLock or acquireLocks.
	 * @return <b>true</b> if it succeeds, <b>false</b> otherwise.
	 */
	public boolean releaseGenericLock(InterProcessLock lock){
		if(lock instanceof InterProcessMutex)
			return releaseLock((InterProcessMutex) lock);
		else if(lock instanceof InterProcessMultiLock)
			return releaseLocks((InterProcessMultiLock) lock);
		else{
			log.error("{} - The provided lock is not an instance of supported locks",
					Thread.currentThread().getName());
			return false;
		}
	}
	
	/**
	 * Watch connection state changes. 
	 * If a SUSPENDED state is reported you cannot be certain that you still hold the lock unless you subsequently receive a RECONNECTED state. 
	 * If a LOST state is reported it is certain that you no longer hold the lock.
	 * http://curator.apache.org/apidocs/org/apache/curator/framework/state/ConnectionState.html
	 * @param listener The listener
	 */
	public void addConnectionStateListener(ConnectionStateListener listener){
		if(clientsMap!=null && clientsMap.get(dataMigrationId)!=null){
			clientsMap.get(dataMigrationId).getConnectionStateListenable().addListener(listener);
		}
	}
}
