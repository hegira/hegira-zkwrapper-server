/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper.dlm;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VDPlockManager implements ILockManager{
	private static String connectString;
	private static ConcurrentHashMap<String, CuratorFramework> clientsMap;
	
	private static VDPlockManager instance = null;
	private static Object instance_lock = new Object();
	
	private String dlm_basepath = "/hegira/VDPdlm";
	private String dataMigrationId;
	private static Logger log = LoggerFactory.getLogger(VDPlockManager.class);
	
	private VDPlockManager(CuratorFramework client, String dataMigrationId){
		VDPlockManager.connectString = client.getZookeeperClient().getCurrentConnectionString();
		clientsMap = new ConcurrentHashMap<String, CuratorFramework>(1);
		this.dataMigrationId = dataMigrationId;
		if(dataMigrationId != null && client != null)
			clientsMap.put(dataMigrationId, client);
	}
	
	private VDPlockManager(String connectString, String dataMigrationId){
		this(CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3)), dataMigrationId);
	}
	
	/**
	 * Checks whether the instance is connected to ZooKeeper.
	 * @param dataMigrationId The data migration id.
	 * @return The result
	 */
	public static boolean isConnected(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		return clientsMap!= null && clientsMap.get(dataMigrationId) != null && 
				clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED) ? 
						true : false;
	}
	
	/**
	 * Creates a new, or retrieves an already existing, {@link VDPlockManager} instance.
	 * When creating a new instance, a connection to ZooKeeper (one for each dataMigrationId) is established and it is not
	 * released until disconnect() is explicitly called on the instance.
	 * @param cs ZooKeeper connect string (host:port)
	 * @param dataMigrationId The id of the data migration
	 * @return The instance.
	 */
	public static VDPlockManager getInstance(String cs, String dataMigrationId){
		if(cs==null || dataMigrationId == null)
			throw new IllegalArgumentException("Zookeeper connect string can't be null");
		synchronized(instance_lock){
			if(instance == null && connectString==null){
				instance = new VDPlockManager(cs, dataMigrationId);
			}else if(!cs.equals(connectString)){
				//connection string has changed
				disconnect_static(dataMigrationId);
				instance = new VDPlockManager(cs, dataMigrationId);
			}
			//if it was manually disconnected, it should reconnect
			if(!isConnected(dataMigrationId))
				connect(dataMigrationId);
		}
		return instance;
	}
	
	/**
	 * Creates a new, or retrieves an already existing, {@link VDPlockManager} instance.
	 * When creating a new instance, a connection to ZooKeeper (one for each dataMigrationId) is established and it is not
	 * released until disconnect() is explicitly called on the instance.
	 * @param client {@link CuratorFramework} object
	 * @param dataMigrationId The id of the data migration
	 * @return The instance.
	 */
	public static VDPlockManager getInstance(CuratorFramework client, String dataMigrationId){
		if(client==null || dataMigrationId == null)
			throw new IllegalArgumentException("Wrong parameters");
		synchronized(instance_lock){
			if(instance == null){
				instance = new VDPlockManager(client, dataMigrationId);
			}else if(clientsMap!=null && clientsMap.get(dataMigrationId) != null && 
					!clientsMap.get(dataMigrationId).equals(client)){
				//connection string has changed
				disconnect_static(dataMigrationId);
				instance = new VDPlockManager(client, dataMigrationId);
			}
			//if it was manually disconnected, it should reconnect
			if(!isConnected(dataMigrationId))
				connect(dataMigrationId);
		}
		return instance;
	}
	
	private static void connect(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		if(isConnected(dataMigrationId)){
			log.warn("{} - Already connected.",
					Thread.currentThread().getName());
			return;
		}
		while(!clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED))
			clientsMap.get(dataMigrationId).start();
		log.debug("{} - Connected.",
					Thread.currentThread().getName());
	}
	
	/**
	 * Releases the connection for the given data migration id.
	 * @param dataMigrationId The migration id.
	 */
	public void disconnect(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		disconnect_static(dataMigrationId);
	}
	
	private static void disconnect_static(String dataMigrationId){
		if(dataMigrationId == null)
			throw new IllegalArgumentException("Data migration Id can't be null");
		if(!isConnected(dataMigrationId)) return;
		while(clientsMap.get(dataMigrationId).getState().equals(CuratorFrameworkState.STARTED))
			clientsMap.get(dataMigrationId).close();
		clientsMap.remove(dataMigrationId);
		log.debug("{} - Disconnected.",
					Thread.currentThread().getName());
	}
	
	private String getPath(String tableName, Integer VDPid){
		if(tableName==null || VDPid<0)
			throw new IllegalArgumentException("Wrong parameters");
		return dlm_basepath+"/"+dataMigrationId+"/"+tableName+"/"+VDPid;
	}
	
	/**
	 * Tries to acquire an exclusive, reentrant lock on the given VDPid, for the time specified by the 
	 * parameters time and unit.
	 * @param tableName The name of the table containing the sequentialId element.
	 * @param VDPid The VDPid characterizing the virtual data partition.
	 * @param time The amount of time to wait for acquiring the exclusive lock.
	 * @param unit The measurement unit for the time parameter.
	 * @return If acquired returns the mutex, otherwise, if an error occurs, it returns <b>null</b>
	 * @throws IllegalStateException If a lock cannot be obtained within the given amount of time, this exception is thrown.
	 */
	public InterProcessMutex acquireLock(String tableName, Integer VDPid, long time, TimeUnit unit)
		throws IllegalStateException {
		if(tableName==null || VDPid<0 || time <= 0 || unit==null)
			throw new IllegalArgumentException("Wrong parameters");
		if(!isConnected(dataMigrationId)){
			log.error("{} - Not connected",
					Thread.currentThread().getName());
			return null;
		}
		
		InterProcessMutex mutex = new InterProcessMutex(clientsMap.get(dataMigrationId), getPath(tableName, VDPid));
		try {
			if (!mutex.acquire(time, unit)){
				throw new IllegalStateException(Thread.currentThread().getName() + " could not acquire the lock");
			}
			return mutex;
		} catch (IllegalStateException e) {
			throw e;
		} catch (Exception e) {
			log.error("{} - Error acquiring the lock for resource {}/{}",
					Thread.currentThread().getName(),
					tableName, VDPid, e);
		}
		return mutex=null;
	}
	
	/**
	 * Releases the lock on the given mutex.
	 * @param mutex The mutex obtained calling the acquireLock method.
	 * @return <b>true</b> if it succeeds, <b>false</b> otherwise. 
	 */
	public boolean releaseLock(InterProcessMutex mutex){
		if(mutex==null){
			throw new IllegalArgumentException("Wrong parameter");
		}
		try {
			mutex.release();
			mutex=null;
			return true;
		} catch (Exception e) {
			log.error("{} - Error releasing the lock",
					Thread.currentThread().getName(), e);
		}
		return false;
	}
	
	/**
	 * Watch connection state changes. 
	 * If a SUSPENDED state is reported you cannot be certain that you still hold the lock unless you subsequently receive a RECONNECTED state. 
	 * If a LOST state is reported it is certain that you no longer hold the lock.
	 * http://curator.apache.org/apidocs/org/apache/curator/framework/state/ConnectionState.html
	 * @param listener The listener
	 */
	public void addConnectionStateListener(ConnectionStateListener listener){
		if(clientsMap!=null && clientsMap.get(dataMigrationId)!=null){
			clientsMap.get(dataMigrationId).getConnectionStateListenable().addListener(listener);
		}
	}
}
