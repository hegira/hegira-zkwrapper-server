/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper;

import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

import java.io.Serializable;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * @author Marco Scavuzzo
 *
 */
public class MigrationStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	private int lastSeqNr;
	public enum VDPstatus{NOT_MIGRATED, UNDER_MIGRATION, MIGRATED, SYNC}
	private HashMap<Integer,StateMachine> VDPs;
	private static Logger log = Logger.getLogger(MigrationStatus.class);
	
	/**
	 * Creates a new MigrationStatus and populates the Maps of VDPs, 
	 * setting all the statuses to NOT_MIGRATED.
	 * @param lastSeqNr The last sequence number generated till now
	 * @param no_vdps The total number of VDPs the MigrationStatus should contain.
	 */
	public MigrationStatus(int lastSeqNr, int no_vdps) {
		this.lastSeqNr = lastSeqNr;
		VDPs = new HashMap<Integer,StateMachine>(no_vdps);
		for(int i=0;i<no_vdps;i++){
			//log.debug("Putting state machine for VDP "+i);
			StateMachine sm = new StateMachine();
			VDPs.put(i, sm);
		}
	}
	
	/**
	 * Retrieves the status of a given VDP
	 * @param vdpId The id of the VDP
	 * @return The StateMachine representing the status of the VDP
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public StateMachine getVDPstatus(int vdpId) throws OutOfSnapshotException{
		StateMachine sm = VDPs.get(Integer.valueOf(vdpId));
		if(sm==null)
			throw new OutOfSnapshotException("VDP "+vdpId+" not found!");
		return sm;
	}
	
	/**
	 * Tries to change the status of the given VDP to SYNC.
	 * If it succeeds it returns SYNC.
	 * @param vdpId The VDP id that one wants to synchronize.
	 * @param type The type of subscriber that is requesting to synchronize (e.g., SOURCE, DESTINATION, SYNC_ONLY). Notice that only the SOURCE subscriber will be able to change the status to SYNC.
	 * @return The VDPstatus after having applied the sync(type) transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus syncVDP(int vdpId, SubscriberType type) throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus(Integer.valueOf(vdpId));
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.sync(type);
			VDPs.put(Integer.valueOf(vdpId), sm);
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP after having performed the synchronization.
	 * If it succeeds it returns the status which resulted after having applied the finish_sync transition to the state machine.
	 * @param vdpId The VDP id that one wants to finish synchronizing.
	 * @return The VDPstatus after having applied the finish_sync transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus finish_syncVDP(int vdpId) throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus(Integer.valueOf(vdpId));
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.finish_sync();
			VDPs.put(Integer.valueOf(vdpId), sm);
		}catch(UnsupportedOperationException e){
			log.warn(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP to UNDER_MIGRATION, provided that the previous state was NOT_MIGRATED.
	 * If it succeeds it returns UNDER_MIGRATION.
	 * @param vdpId The VDP id that one wants to migrate.
	 * @return The VDPstatus after having applied the migrate transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus migrateVDP(int vdpId) throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus(Integer.valueOf(vdpId));
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.migrate();
			VDPs.put(Integer.valueOf(vdpId), sm);
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP to MIGRATED, provided that the previous state was UNDER_MIGRATION.
	 * If it succeeds it returns MIGRATED.
	 * @param vdpId The VDP id that one wants to finish migrating.
	 * @return The VDPstatus after having applied the finish_migration transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus finish_migrateVDP(int vdpId) throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus(Integer.valueOf(vdpId));
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.finish_migration();
			VDPs.put(Integer.valueOf(vdpId), sm);
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}

	/**
	 * Gets the last sequence number generated by ZooKeeper for the given table.
	 * @return The last sequence number.
	 */
	public int getLastSeqNr() {
		return lastSeqNr;
	}

	/**
	 * Sets the last sequence number for the given table.
	 * @param lastSeqNr The last sequence number.
	 */
	public void setLastSeqNr(int lastSeqNr) {
		this.lastSeqNr = lastSeqNr;
	}

	/**
	 * Gets the statuses of all VDPs for the given table.
	 * @return An HashMap whose Keys are the VDPids and Values the StateMachines for each VDP.
	 */
	public HashMap<Integer, StateMachine> getVDPs() {
		return VDPs;
	}

	/**
	 * Sets the statuses of all VDPs for the given table.
	 * @param vDPs An HashMap whose Keys are the VDPids and Values the StateMachines for each VDP.
	 */
	public void setVDPs(HashMap<Integer, StateMachine> vDPs) {
		VDPs = vDPs;
	}
	
	/**
	 * Calculates the total number of VDPs contained by this MigrationStatus
	 * @param VDPsize
	 * @return
	 */
	public int getTotalVDPs(int VDPsize){
		//It could also be calculated by counting the number of elements inside HashMap VDPs
		return VdpUtils.getTotalVDPs(lastSeqNr, VDPsize);
	}
}
