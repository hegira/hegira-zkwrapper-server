/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.curator.CuratorZookeeperClient;
import org.apache.curator.RetryLoop;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.locks.InterProcessSemaphoreMutex;
import org.apache.curator.framework.recipes.shared.SharedCount;
import org.apache.curator.framework.recipes.shared.SharedCountListener;
import org.apache.curator.framework.recipes.shared.SharedValue;
import org.apache.curator.framework.recipes.shared.SharedValueListener;
import org.apache.curator.framework.recipes.shared.VersionedValue;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.ZKPaths;
import org.apache.log4j.Logger;
import org.apache.zookeeper.ZooKeeper;

/**
 * @author Marco Scavuzzo
 *
 */
public class ZKserver {
	
	private String connectString;
	private String tables_path = "/hegira/migrations";
	private String vdpSizePath = "/hegira/VDPsize";
	private String queryLockPath = "/hegira/queryLock";
	private CuratorFramework client;
	private ConcurrentHashMap<String, SharedValue> tables_statuses;
	private SharedCount vdpSizeCount;
	private SharedCount queryLock;
	//HashMap containing a lock per each migration table
	private ConcurrentHashMap<String,InterProcessSemaphoreMutex> locks;
	private static Logger logger = Logger.getLogger(ZKserver.class);
	
	/**
	 * Creates a connection to the ZooKeeper server and instantiates 
	 * a Map that contains the SharedCounts representing the VDPid under migration
	 * for each table in the source database.
	 */
	public ZKserver(String connectString) {
		this.connectString = connectString;
		client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		while(!client.getState().equals(CuratorFrameworkState.STARTED))
			client.start();
		tables_statuses = new ConcurrentHashMap<String, SharedValue>();
		locks = new ConcurrentHashMap<String, InterProcessSemaphoreMutex>();
	}
	
	/**
	 * Tries to acquire a lock on the given table.
	 * @param tableName The name of the table.
	 * @return <b>true</b> if the lock has been acquired, <b>false</b> otherwise
	 * @throws Exception
	 */
	public synchronized boolean acquireLock(String tableName) throws Exception{
		InterProcessSemaphoreMutex lock = locks.get(tableName);
		if(lock==null){
			lock = new InterProcessSemaphoreMutex(client, tables_path+"/"+tableName);
			locks.put(tableName, lock);
		}
		return lock.acquire(300, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Releases a lock on a table if previously acquired.
	 * @param tableName The name of the table.
	 * @throws Exception
	 */
	public synchronized void releaseLock(String tableName) throws Exception{
		InterProcessSemaphoreMutex lock = locks.get(tableName);
		if(lock!=null)
			lock.release();
	}
	
	public synchronized boolean isTableLocked(String tableName) {
		if(locks==null) return false;
		InterProcessSemaphoreMutex lock = locks.get(tableName);
		if(lock==null){
			lock = new InterProcessSemaphoreMutex(client, tables_path+"/"+tableName);
			locks.put(tableName, lock);
		}
		return lock.isAcquiredInThisProcess();
	}
	
	/**
	 * Returns the proper SharedValue representing a
	 * MigrationStatus for a given table.
	 * If it doesn't exist it creates a new one and puts it in the
	 * tables_statuses.
	 * @param tableName The name of the table
	 * @return The SharedValue representing the MigrationStatus.
	 */
	private synchronized SharedValue getMigrationStatusSharedValue(String tableName){
		SharedValue sv = tables_statuses.get(tableName);
		String path = tables_path+"/"+tableName;
		if(sv==null){
			sv = new SharedValue(client, path, new byte[0]);
			try {
				sv.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			tables_statuses.put(tableName, sv);
		}
		return sv;
	}

	/**
	 * Returns the proper SharedValue representing a
	 * MigrationStatus for a given table.
	 * It ALWAYS gets an updated version from ZooKeeper and puts it in the
	 * tables_statuses.
	 * @param tableName The name of the table
	 * @return	The SharedValue representing the MigrationStatus.
	 */
	private synchronized SharedValue getFreshMigrationStatusSharedValue(String tableName){
		String path = tables_path+"/"+tableName;
		SharedValue sv = new SharedValue(client, path, new byte[0]);
		try {
			sv.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tables_statuses.put(tableName, sv);
		
		return sv;
	}
	
	/**
	 * Sets the migration status for a given table.	
	 * @param tableName The table name.
	 * @param status The migration status
	 * @return <b>true</b> if the operation , <b>false</b> otherwise. 
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public synchronized boolean setMigrationStatus(String tableName, MigrationStatus status) throws Exception{
		SharedValue sv = getMigrationStatusSharedValue(tableName);
		VersionedValue<byte[]> status_vv = sv.getVersionedValue();
		boolean setted = sv.trySetValue(status_vv, serialize(status));
		//updating local map
		if(setted)
			tables_statuses.put(tableName, sv);
		
		return setted;
	}
	
	/**
	 * Sets the migration status for a given table substituting the most recent version if necessary.
	 * @param tableName The table name.
	 * @param status The migration status.
	 * @return <b>true</b> if the operation succeeded, <b>false</b> otherwise.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public synchronized boolean setFreshMigrationStatus(String tableName, MigrationStatus status) throws Exception{
		SharedValue sv = getFreshMigrationStatusSharedValue(tableName);
		VersionedValue<byte[]> status_vv = sv.getVersionedValue();
		boolean setted = sv.trySetValue(status_vv, serialize(status));
		//updating local map
		if(setted)
			tables_statuses.put(tableName, sv);
		
		return setted;
	}
	
	/**
	 * To be used when recovering from a crash, in order to reconstruct previous migration status, in conjunction with getMigrationStatus
	 * @return Returns the list of tables created before starting the migration.
	 * @throws Exception Non retryable error.
	 */
	public List<String> getMigrationPaths() throws Exception{
		CuratorZookeeperClient zClient = client.getZookeeperClient();
		RetryLoop retryLoop = zClient.newRetryLoop();
		List<String> list = new ArrayList<String>();
		while ( retryLoop.shouldContinue() )
		{
		   try{
			   ZooKeeper zk = zClient.getZooKeeper();
			   list = ZKPaths.getSortedChildren(zk, tables_path);
		       // it's important to re\-get the ZK instance as there may have been an error and the instance was re\-created
		       zk = zClient.getZooKeeper();
		       retryLoop.markComplete();
		   }catch ( Exception e ){
		       retryLoop.takeException(e);
		   }
		}
		return list;
	}
	
	/**
	 * Retrieves the MigrationStatus for a given table.
	 * @param tableName The name of the table.
	 * @return	The MigrationStatus object.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	@Deprecated
	public MigrationStatus getMigrationStatus(String tableName) throws ClassNotFoundException, IOException{
		SharedValue migrationStatusSharedValue = getMigrationStatusSharedValue(tableName);
		MigrationStatus deserialized = 
				(MigrationStatus) deserialize(migrationStatusSharedValue.getValue());
		
		return deserialized;
	}

	/**
	 * Gets the most updated MigrationStatus for the given table from ZooKeeper and ALWAYS updates the local value.
	 * @param tableName The table name.
	 * @param listener A listener that is notified of state or value changes or <code>null</code> if no notification is needed. 
	 * @return The MigrationStatus for the given table
	 * @throws ClassNotFoundException Error deserializing the MigrationStatus retrieved from ZooKeeper.
	 * @throws IOException Error deserializing the MigrationStatus retrieved from ZooKeeper.
	 */
	public MigrationStatus getFreshMigrationStatus(String tableName, SharedValueListener listener) 
			throws ClassNotFoundException, IOException{
		SharedValue migrationStatusSharedValue = getFreshMigrationStatusSharedValue(tableName);
		MigrationStatus deserialized = 
				(MigrationStatus) deserialize(migrationStatusSharedValue.getValue());
		if(listener!=null)
			migrationStatusSharedValue.getListenable().addListener(listener);
		
		return deserialized;
	}
	
	/**
	 * Method to be called after the migration process has completed.
	 * It deletes all of the migration statuses from ZooKeeper.
	 * @return <b>true</b> if the operation succeeded, <b>false</b> otherwise;
	 * @throws Exception Non retryable error.
	 */
	public boolean deleteMigrationStatuses() throws Exception{
		CuratorZookeeperClient zClient = client.getZookeeperClient();
		RetryLoop retryLoop = zClient.newRetryLoop();
		boolean deleted = false;
		while ( retryLoop.shouldContinue() )
		{
		   try{
			   ZooKeeper zk = zClient.getZooKeeper();
			   ZKPaths.deleteChildren(zk, tables_path, false);
			   deleted = true;
			   tables_statuses.clear();
		       // it's important to re\-get the ZK instance as there may have been an error and the instance was re\-created
		       zk = zClient.getZooKeeper();
		       retryLoop.markComplete();
		   }catch ( Exception e ){
		       retryLoop.takeException(e);
		   }
		}
		return deleted;
	}
	
	/**
	 * Sets the size of **all** VDPs for a migration task.
	 * @param p The exponent for the number 10 (i.e. 10^<b>p</b>).
	 * @return <b>true</b> if the operation succeeded, <b>false</b> otherwise;
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public synchronized boolean setVDPsize(int p) throws Exception{
		vdpSizeCount = new SharedCount(client, vdpSizePath, 0);
		vdpSizeCount.start();
		VersionedValue<Integer> vv = vdpSizeCount.getVersionedValue();
		boolean success = vdpSizeCount.trySetCount(vv, p);
		int retries=0;
		while(!success && retries<=3){
			Thread.sleep(1000);
			success = vdpSizeCount.trySetCount(vv, p);
			retries++;
		}
		vdpSizeCount.close();
		return success;
	}

	/**
	 * Gets the size relative to **all** VDPs for the migration task currently in place.
	 * @return The VDPs size.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public int getVDPsize() throws Exception{
		vdpSizeCount = new SharedCount(client, vdpSizePath, 0);
		vdpSizeCount.start();
		int count = vdpSizeCount.getCount();
		vdpSizeCount.close();
		return count;
	}
	
	/**
	 * Sets the synchronize status to true, so that listeners will be notified of it.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	protected void synchronize() throws Exception{
		ZKclient zKclient = new ZKclient(connectString);
		zKclient.synchronize();
		zKclient.close();
	}
	
	/**
	 * Sets the synchronize status to false, so that listeners will be notified of it.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	protected void unsynchronize() throws Exception{
		ZKclient zKclient = new ZKclient(connectString);
		zKclient.unsynchronize();
		zKclient.close();
	}
	
	private synchronized void setLockQueries(boolean value) throws Exception{
		queryLock = new SharedCount(client, queryLockPath, BooleanUtil.fromBoolean(value));
		queryLock.start();
		VersionedValue<Integer> vv = queryLock.getVersionedValue();
		boolean success = queryLock.trySetCount(vv, BooleanUtil.fromBoolean(value));
		int retries=0;
		while(!success && retries<=3){
			Thread.sleep(1000);
			success = queryLock.trySetCount(vv, BooleanUtil.fromBoolean(value));
			retries++;
		}
		queryLock.close();
	}
	
	/**
	 * Sets the lock on query propagation
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public void lockQueries() throws Exception{
		setLockQueries(true);
	}
	
	/**
	 * Removes the lock on the query propagation
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public void unlockQueries() throws Exception{
		setLockQueries(false);
	}
	
	/**
	 * Says if queries should be locked, typically because the snapshot creation may be in progress.
	 * @param listener A listener which is asynchronously notified for lock changes.
	 * @return <b>true</b> if queries are blocked, <b>false</b> otherwise
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public boolean isLocked(SharedCountListener listener) throws Exception{
		queryLock = new SharedCount(client, queryLockPath, BooleanUtil.fromBoolean(false));
		queryLock.start();
		boolean synching = BooleanUtil.toBoolean(queryLock.getCount());
		if(listener!=null)
			queryLock.addListener(listener);
		queryLock.close();
		return synching;
	}
	
	/**
	 * Closes all SharedValues representing tables Migration statuses.
	 * Closes CuratorFramework object.
	 */
	public synchronized void close(){
		//first lets close the shared values
		Set<String> keys = tables_statuses.keySet();
		for(String key : keys){
			SharedValue sv = tables_statuses.get(key);
			if(sv!=null){
				try {
					sv.close();
				} catch (IOException e) {
					logger.warn("Couldn't close SharedValue associated to table: "+key, e);
				}
			}
		}
		//...then the client
		if(client.getState().equals(CuratorFrameworkState.STARTED) && 
				!client.getState().equals(CuratorFrameworkState.STOPPED)){
			client.close();
		}
		this.client = null;
	}
	
	
	private byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}
	
	public Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return o.readObject();
	}
	
	public ZooKeeper getZooKeeper(){
		if(client!=null){
			try {
				return client.getZookeeperClient().getZooKeeper();
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
}
