/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper;

import java.io.Serializable;

import org.apache.log4j.Logger;

import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * Status of a single VDP
 * @author Marco Scavuzzo
 *
 */
public class VDPmigrationStatus implements Serializable{
	private static final long serialVersionUID = 1L;
	private int vdpId, vdpNo;
	private int table_lastSeqNr;
	private StateMachine VDPsm;
	private static Logger log = Logger.getLogger(VDPmigrationStatus.class);
	
	/**
	 * Creates a new MigrationStatus and populates the Maps of VDPs, 
	 * setting all the statuses to NOT_MIGRATED.
	 * @param lastSeqNr The last sequence number generated till now for the table containing this VDP
	 */
	public VDPmigrationStatus(int vdpId, int lastSeqNr, int vdpNo) {
		this.table_lastSeqNr = lastSeqNr;
		this.vdpId = vdpId;
		this.vdpNo = vdpNo;
		if(vdpId<vdpNo)
			this.VDPsm = new StateMachine();
	}
	
	public VDPmigrationStatus(VDPmigrationStatus other) throws OutOfSnapshotException{
		this.vdpId = other.vdpId;
		this.vdpNo = other.vdpNo;
		this.table_lastSeqNr = other.table_lastSeqNr;
		this.VDPsm = new StateMachine(State.valueOf(other.getVDPstatus().getCurrentState().name()));
	}
	
	/**
	 * Retrieves the status of a given VDP
	 * @param vdpId The id of the VDP
	 * @return The StateMachine representing the status of the VDP
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public StateMachine getVDPstatus() throws OutOfSnapshotException{
		if(VDPsm==null)
			throw new OutOfSnapshotException("VDP not found!");
		return VDPsm;
	}
	
	/**
	 * Tries to change the status of the given VDP to SYNC.
	 * If it succeeds it returns SYNC.
	 * @param vdpId The VDP id that one wants to synchronize.
	 * @param type The type of subscriber that is requesting to synchronize (e.g., SOURCE, DESTINATION, SYNC_ONLY). Notice that only the SOURCE subscriber will be able to change the status to SYNC.
	 * @return The VDPstatus after having applied the sync(type) transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus syncVDP(SubscriberType type) throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus();
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.sync(type);
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP after having performed the synchronization.
	 * If it succeeds it returns the status which resulted after having applied the finish_sync transition to the state machine.
	 * @param vdpId The VDP id that one wants to finish synchronizing.
	 * @return The VDPstatus after having applied the finish_sync transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus finish_syncVDP() throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus();
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.finish_sync();
		}catch(UnsupportedOperationException e){
			log.warn(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP to UNDER_MIGRATION, provided that the previous state was NOT_MIGRATED.
	 * If it succeeds it returns UNDER_MIGRATION.
	 * @param vdpId The VDP id that one wants to migrate.
	 * @return The VDPstatus after having applied the migrate transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus migrateVDP() throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus();
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.migrate();
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}
	
	/**
	 * Tries to change the status of the given VDP to MIGRATED, provided that the previous state was UNDER_MIGRATION.
	 * If it succeeds it returns MIGRATED.
	 * @param vdpId The VDP id that one wants to finish migrating.
	 * @return The VDPstatus after having applied the finish_migration transition to the state machine.
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, has been requested.
	 */
	public VDPstatus finish_migrateVDP() throws OutOfSnapshotException{
		StateMachine sm = getVDPstatus();
		State returnState = sm.getCurrentState();
		try{
			returnState = sm.finish_migration();
		}catch(UnsupportedOperationException e){
			log.error(e.getMessage());
		}
		return VDPstatus.valueOf(returnState.name());
	}

	/**
	 * Gets the last sequence number generated by ZooKeeper for the given table.
	 * @return The last sequence number.
	 */
	public int getLastSeqNr() {
		return table_lastSeqNr;
	}

	/**
	 * Sets the last sequence number for the given table.
	 * @param lastSeqNr The last sequence number.
	 */
	public void setLastSeqNr(int lastSeqNr) {
		this.table_lastSeqNr = lastSeqNr;
	}

	
	/**
	 * Calculates the total number of VDPs contained by this {@link VDPmigrationStatus}
	 * @param VDPsize
	 * @return
	 */
	public int getTotalVDPs(int VDPsize){
		//It could also be calculated by counting the number of elements inside HashMap VDPs
		return VdpUtils.getTotalVDPs(table_lastSeqNr, VDPsize);
	}
	
	/**
	 * Gets the total number of VDPs contained by this {@link VDPmigrationStatus} as it was decleraded when the status was created
	 * @param VDPsize
	 * @return
	 */
	public int getTotalVDPs(){
		//It could also be calculated by counting the number of elements inside HashMap VDPs
		return vdpNo;
	}
	
	public void resetStateMachine(){
		this.VDPsm = new StateMachine();
	}
	
	/**
	 * Sets the state machine, representing the VDP status, to a custom value without checking the transactions!
	 * @param sm
	 */
	public void setStateMachine(StateMachine sm){
		this.VDPsm = sm;
	}
}
