/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper;

import java.util.Map;

import it.polimi.hegira.zkWrapper.statemachine.MigrationComponentStatus;
import net.killa.kept.KeptConcurrentMap;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.ZooDefs.Ids;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class ZKcomponentStatus {
	private static Logger log = LoggerFactory.getLogger(ZKcomponentStatus.class);
	private final String path = "/hegiraComponentsStatuses";
	private Map<String,MigrationComponentStatus> map;
	
	private CuratorFramework client;
	
	public ZKcomponentStatus(CuratorFramework client) throws KeeperException, InterruptedException {
		//String connectString = client.getZookeeperClient().getCurrentConnectionString();
		connect();
		initMap();
	}
	
	public ZKcomponentStatus(String connectString) throws KeeperException, InterruptedException {
		if(connectString==null)
			throw new IllegalArgumentException();
		
		client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		connect();
		initMap();
	}
	
	private void initMap() throws KeeperException, InterruptedException {
		this.map = new KeptConcurrentMap<MigrationComponentStatus>(MigrationComponentStatus.class,
				getZooKeeperNativeClient(),
				path,
				Ids.OPEN_ACL_UNSAFE, 
				CreateMode.PERSISTENT);
	}
	
	private ZooKeeper getZooKeeperNativeClient(){
		if(client!=null && isConnected()){
			try {
				return client.getZookeeperClient().getZooKeeper();
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
	
	public boolean isConnected(){
		return client.getState().equals(CuratorFrameworkState.STARTED) ? 
						true : false;
	}
	
	public void connect(){
		if(isConnected()){
			log.warn("{} - Already connected.",
					Thread.currentThread().getName());
			return;
		}
		while(!isConnected())
			client.start();
		log.debug("{} - Connected.",
					Thread.currentThread().getName());
	}
	
	public void disconnect(){
		if(!isConnected()) return;
		while(isConnected())
			client.close();
		log.debug("{} - Disconnected.",
					Thread.currentThread().getName());
	}
	
	public void addConnectionStateListener(ConnectionStateListener listener){
		if(client!=null && listener!=null)
			client.getConnectionStateListenable().addListener(listener);
	}
	
	public synchronized void setMigrationComponentStatus(MigrationComponentStatus status) {
		if(status==null)
			throw new IllegalArgumentException();
		map.put(generateMapKey(status), status);
	}
	
	public synchronized MigrationComponentStatus getMigrationComponentStatus(String componentName, int mId) {
		if(componentName==null || mId<0)
			throw new IllegalArgumentException();
		return map.get(generateMapKey(componentName, mId));
	}
	
	private String generateMapKey(MigrationComponentStatus status) {
		if(status==null)
			return null;
		return status.getComponentName()+status.getmId();
	}
	
	private String generateMapKey(String componentName, int mId) {
		if(componentName==null || mId<0)
			return null;
		return componentName+mId;
	}
}
