/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper.statemachine;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import it.polimi.hegira.zkWrapper.statemachine.MigrationComponentStatus.MigrationComponentState.State;

/**
 * @author Marco Scavuzzo
 */
public class MigrationComponentStatus implements Serializable {
	private static final long serialVersionUID = -8181942841220300216L;
	private transient static Logger log = Logger.getLogger(MigrationComponentStatus.class);
	private int mId;
	private String componentName;
	private long last_change;
	private Map<State, MigrationComponentState> states;
	
	public MigrationComponentStatus(int mId, String componentName){
		if(mId<0 || componentName==null)
			throw new IllegalArgumentException();
		this.mId = mId;
		this.componentName = componentName;
		this.last_change = -1;
		states=new HashMap<State, MigrationComponentState>();
		
		for(State st : MigrationComponentState.State.values()){
			MigrationComponentState state = new MigrationComponentState();
			state.setState(st);
			state.setValue(false);
			state.setTimestamp(-1);
			states.put(st, state);
		}
	}
	
	private void setMigrationComponentState(State st, boolean active, Long timestamp){
		if(st==null)
			throw new IllegalArgumentException();
		try{
			MigrationComponentState migCmpState = states.get(st);
			migCmpState.setValue(active);
			if(timestamp!=null){
				migCmpState.setTimestamp(timestamp);
				last_change = timestamp;
			}else{
				last_change = System.currentTimeMillis(); 
			}
			states.put(st, migCmpState);
		} catch(NullPointerException e){
			log.error("MigrationComponentStatus "+st+" uncorrectly instantiated!");
		}
	}
	
	public void setActive(){
		setMigrationComponentState(State.COMPONENT_ACTIVE, true, System.currentTimeMillis());
	}
	
	public void setInactive(){
		setMigrationComponentState(State.COMPONENT_ACTIVE, false, System.currentTimeMillis());
	}
	
	public void startMigration(){
		setMigrationComponentState(State.MIG_STARTED, true, System.currentTimeMillis());
	}
	
	public void finishMigration(){
		//leaving the original timestamp. Just changing the state
		setMigrationComponentState(State.MIG_STARTED, false, null);
		setMigrationComponentState(State.MIG_FINISHED, true, System.currentTimeMillis());
	}
	
	public void pauseMigration() throws IllegalStateException{
		if(states.get(State.MIG_STARTED).getValue() 
				&&  states.get(State.COMPONENT_ACTIVE).getValue() 
				&& !states.get(State.MIG_FINISHED).getValue()){
			setMigrationComponentState(State.PAUSED, true, System.currentTimeMillis());
		} else {
			throw new IllegalStateException("Cannot pause migration "+mId+". Inconsistent status: \n"+this.toString());
		}
	}
	
	public void unpauseMigration() throws IllegalStateException{
		if(states.get(State.MIG_STARTED).getValue() 
				&&  states.get(State.COMPONENT_ACTIVE).getValue() 
				&& !states.get(State.MIG_FINISHED).getValue()
				&&  states.get(State.PAUSED).getValue()){
			setMigrationComponentState(State.PAUSED, false, System.currentTimeMillis());
		} else {
			throw new IllegalStateException("Cannot unpause migration "+mId+". Inconsistent status: \n"+this.toString());
		}
	}
	
	public void slowdownMigration() throws IllegalStateException{
		if(states.get(State.MIG_STARTED).getValue() 
				&&  states.get(State.COMPONENT_ACTIVE).getValue() 
				&& !states.get(State.MIG_FINISHED).getValue()
				&& !states.get(State.PAUSED).getValue()){
			setMigrationComponentState(State.SLOWED, true, System.currentTimeMillis());
		} else {
			throw new IllegalStateException("Cannot slowdown migration "+mId+". Inconsistent status: \n"+this.toString());
		}
	}
	
	public void speedupMigration() throws IllegalStateException{
		if(states.get(State.MIG_STARTED).getValue() 
				&&  states.get(State.COMPONENT_ACTIVE).getValue() 
				&& !states.get(State.MIG_FINISHED).getValue()
				&& !states.get(State.PAUSED).getValue()
				&&  states.get(State.SLOWED).getValue()){
			setMigrationComponentState(State.SLOWED, false, System.currentTimeMillis());
		} else {
			throw new IllegalStateException("Cannot slowdown migration "+mId+". Inconsistent status: \n"+this.toString());
		}
	}
	
	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public long getLast_change() {
		return last_change;
	}

	public void setLast_change(long last_change) {
		this.last_change = last_change;
	}

	public Map<State, MigrationComponentState> getStates() {
		return states;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Migration Id: "+mId+"\n");
		sb.append("Component name: "+componentName+"\n");
		sb.append("Last change: "+last_change+"\n");
		for(MigrationComponentState st : states.values()) {
			sb.append(st+"\n");
		}
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if(!(obj instanceof MigrationComponentStatus))
			return false;
		MigrationComponentStatus other = (MigrationComponentStatus) obj;
		boolean statesEquivalence = true;
		for(MigrationComponentState st : states.values()) {
			MigrationComponentState migCmptState = other.states.get(st.state);
			if(migCmptState==null){
				log.error("Other hasn't state: "+st.state);
				statesEquivalence=false;
				break;
			}
			statesEquivalence &= migCmptState.equals(st);
		}
		
		boolean nativesEquivalence = other.componentName.equals(componentName) 
			&& other.last_change == last_change
			&& other.mId == mId;
		return nativesEquivalence && statesEquivalence;
	}
	
	public final static class MigrationComponentState implements Serializable {
		private static final long serialVersionUID = 3952993334388250117L;

		public enum State {
			COMPONENT_ACTIVE, MIG_STARTED, MIG_FINISHED, PAUSED, SLOWED
		}
		
		private State state;
		private boolean value;
		private long timestamp;
		
		public State getState() {
			return state;
		}
		
		private void setState(State state) {
			if(state==null)
				throw new IllegalArgumentException();
			this.state = state;
		}
		
		public boolean getValue() {
			return value;
		}
		
		private void setValue(boolean value) {
			this.value = value;
		}
		
		public long getTimestamp() {
			return timestamp;
		}
		
		private void setTimestamp(long timestamp) {
			if(timestamp<-1)
				throw new IllegalArgumentException();
			this.timestamp = timestamp;
		}
		
		@Override
		public String toString() {
			String returnString = "State: "+state+"="+value+" at: "+timestamp;
			return returnString;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((state == null) ? 0 : state.hashCode());
			result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
			result = prime * result + (value ? 1231 : 1237);
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if(!(obj instanceof MigrationComponentState))
				return false;
			MigrationComponentState other = (MigrationComponentState) obj;
			boolean returnValue = 
					other.value==value 
					&& other.state.name().equals(state.name())
					&& other.timestamp == timestamp;
			return returnValue;
		}
	}
}
