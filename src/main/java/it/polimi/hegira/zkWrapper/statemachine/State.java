/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper.statemachine;

import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * This enum class represents the States of the State Machine (FSM) representing the status of a VDP.
 * Each State contains methods representing a transition in the FSM.
 * If a transition from a state is allowed then the result of the transition will be the next State, otherwise it returns null.
 * If the transition is a loop, the current State is returned.
 * @author Marco Scavuzzo
 */
public enum State {
	
	NOT_MIGRATED{
		@Override
		protected State migrate() {return UNDER_MIGRATION;}

		@Override
		protected State finish_migration() {return null;}

		@Override
		protected State sync(SubscriberType type) {
			if(type.equals(SubscriberType.SOURCE))
				return SYNC;
			else
				return NOT_MIGRATED;
		}

		@Override
		protected State finish_sync() {return null;}
		
	},
	SYNC{

		@Override
		protected State migrate() {return null;}

		@Override
		protected State finish_migration() {return null;}

		@Override
		protected State sync(SubscriberType type) {return SYNC;}

		@Override
		protected State finish_sync() {return NOT_MIGRATED;}
		
	},
	UNDER_MIGRATION{

		@Override
		protected State migrate() {return null;}

		@Override
		protected State finish_migration() {return MIGRATED;}

		@Override
		protected State sync(SubscriberType type) {return UNDER_MIGRATION;}

		@Override
		protected State finish_sync() {return null;}
		
	},
	MIGRATED{

		@Override
		protected State migrate() {return null;}

		@Override
		protected State finish_migration() {return null;}

		@Override
		protected State sync(SubscriberType type) {return MIGRATED;}

		@Override
		protected State finish_sync() {return MIGRATED;}
		
	};
	
	protected abstract State migrate();
	protected abstract State finish_migration();
	protected abstract State sync(SubscriberType type);
	protected abstract State finish_sync();
}
