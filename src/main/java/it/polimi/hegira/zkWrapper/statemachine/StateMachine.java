/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper.statemachine;

import java.io.Serializable;

import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * @author Marco Scavuzzo
 *
 */
public class StateMachine implements Serializable{

	private State currentState;
	/**
	 * Creates a new State Machine (representing the MigrationStatus)
	 * at its initial status (NOT_MIGRATED)
	 */
	public StateMachine() {
		currentState = State.NOT_MIGRATED;
	}
	
	/**
	 * Creates a State Machine starting from a given state.
	 * @param state The customized initial state.
	 */
	public StateMachine(State state) {
		currentState = state;
	}
	
	/**
	 * Gets State Machine current state.
	 * @return The current state
	 */
	public State getCurrentState(){
		return currentState;
	}
	
	public State migrate(){
		State state = currentState.migrate();
		if(state==null)
			throw new UnsupportedOperationException(
					"Migration not supported on state: "
							+currentState);
		return currentState = state;
	}
	
	public State finish_migration(){
		State state = currentState.finish_migration();
		if(state==null)
			throw new UnsupportedOperationException(
					"Finish Migration not supported on state: "
							+currentState);
		return currentState = state;
	}
	
	public State sync(SubscriberType type){
		State state = currentState.sync(type);
		if(state==null)
			throw new UnsupportedOperationException(
					"Synchronization not supported on state: "
							+currentState);
		return currentState = state;
	}
	
	public State finish_sync(){
		State state = currentState.finish_sync();
		if(state==null)
			throw new UnsupportedOperationException(
					"Finish Synchronization not supported on state: "
							+currentState);
		return currentState = state;
	}
}
