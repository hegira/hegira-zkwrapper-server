/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper;

import java.io.Serializable;
import java.util.Map;

import net.killa.kept.KeptConcurrentMap;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.junit.Test;

public class TestKeptCollections {
	private final String connectString = "localhost:2181";
	
	@Test
	public void testMap(){
		ZKserver zKserver = new ZKserver(connectString);
		ZooKeeper keeper = zKserver.getZooKeeper();
		
		try {
			Map<String, TestObject> map =
					  new KeptConcurrentMap<TestObject>(TestObject.class, keeper, 
							  "/componentName", Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			
			map.put("mId1", new TestObject(1, System.currentTimeMillis(), TestObject.State.STARTED));
			Thread.sleep(50);
			System.out.println("Getting mId1: "+map.get("mId1"));
		} catch (KeeperException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static class TestObject implements Serializable {
		private static final long serialVersionUID = -7298878556678286227L;
		public int mId;
		public long timestamp;
		public enum State {
			STARTED, FINISHED, SLOWED, PAUSED;
		};
		public State state;
		
		public TestObject(int mId, long timestamp, State state){
			this.mId = mId;
			this.timestamp = timestamp;
			this.state = state;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj==null)
				throw new IllegalArgumentException();
			TestObject to = (TestObject) obj;
			boolean test = mId==to.mId && timestamp == to.timestamp && state.name().equals(to.state.name());
			return test;
		}
		
		@Override
		public String toString() {
			return "mId: "+mId
					+" timestamp: "+timestamp
					+" state: "+state;
		}
	}
}
