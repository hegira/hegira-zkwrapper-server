/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import it.polimi.hegira.zkWrapper.dlm.LockManager;

import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.junit.Test;

public class TestLockManager {

	@Test
	public void testAcquireLock() {
		String dmid = ""+System.currentTimeMillis();
		InterProcessMutex lock1 = null, lock2 = null, lock1_1 = null;
		LockManager dlm = null;
		try{
			dlm = LockManager.getInstance("localhost:2181", dmid);
			
			try{
				//check locking on an element.
				lock1 = dlm.acquireLock("prova", 1, 10, TimeUnit.SECONDS);
				assertNotNull(lock1);
				
				try{
					//checking that other elements in the same table are not locked.
					lock2 = dlm.acquireLock("prova", 2, 10, TimeUnit.SECONDS);
					assertNotNull(lock2);
				} finally {
					if(lock2!=null)
						assertTrue(dlm.releaseLock(lock2));
				}
				
				//checking exclusive lock on already locked element.
				IllegalStateException caughtEx = null;
				try{
					lock1_1 = dlm.acquireLock("prova", 1, 2, TimeUnit.SECONDS);
				} catch(IllegalStateException e){
					caughtEx = e;
				}
				assertNotNull(caughtEx);
			} finally {
				//check release lock on first element.
				if(lock1!=null)
					assertTrue(dlm.releaseLock(lock1));
			}
			
			try{
				//trying to get the lock again 
				lock1_1 = dlm.acquireLock("prova", 1, 10, TimeUnit.SECONDS);
				assertNotNull(lock1_1);
			} finally {
				//check release locks.
				if(lock1_1!=null)
					assertTrue(dlm.releaseLock(lock1_1));
			}
		} finally {
			if(dlm!=null)
				dlm.disconnect(dmid);
		}
	}
}
