/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper;

import static org.junit.Assert.*;
import it.polimi.hegira.zkWrapper.statemachine.MigrationComponentStatus;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestZkComponentStatus {
	static ZKcomponentStatus zk;
	private static Logger log = LoggerFactory.getLogger(TestZkComponentStatus.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		zk = new ZKcomponentStatus("localhost:2181");
	}

	@Test
	public void test() throws InterruptedException {
		MigrationComponentStatus src1 = new MigrationComponentStatus(1, "SRC");
		src1.setActive();
		zk.setMigrationComponentStatus(src1);
		log.debug("Set SRC 1 to Active");
		Thread.sleep(150);
		MigrationComponentStatus src1zk = zk.getMigrationComponentStatus("SRC", 1);
		log.debug("SRC1 got from ZK:\n{}",src1zk);
		assertEquals(src1, src1zk);
		
		
		src1zk.startMigration();
		zk.setMigrationComponentStatus(src1zk);
		log.debug("Set SRC 1 (zk) to start migration");
		Thread.sleep(150);
		MigrationComponentStatus src1zk_1 = zk.getMigrationComponentStatus("SRC", 1);
		log.debug("SRC1 got again from ZK:\n{}",src1zk_1);
		assertEquals(src1zk, src1zk_1);
	}
	
	@AfterClass
	public static void tear() throws Exception {
		log.debug("Disconnecting");
		zk.disconnect();
	}
}
