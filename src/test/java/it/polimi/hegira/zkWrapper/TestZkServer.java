/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper;

import static org.junit.Assert.*;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.shared.SharedCountListener;
import org.apache.curator.framework.recipes.shared.SharedCountReader;
import org.apache.curator.framework.recipes.shared.SharedValueListener;
import org.apache.curator.framework.recipes.shared.SharedValueReader;
import org.apache.curator.framework.state.ConnectionState;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * @author Marco Scavuzzo
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestZkServer {
	String connectString = "localhost:2181";

	/**
	 * Test method for 
	 * {@link it.polimi.hegira.zkWrapper.ZKserver#setMigrationStatus(java.lang.String, it.polimi.hegira.zkWrapper.MigrationStatus)}.
	 * and
	 * {@link it.polimi.hegira.zkWrapper.ZKserver#getMigrationStatus(java.lang.String)}.
	 * @throws Exception 
	 */
	@Ignore
	public void AtestSetMigrationStatusStringInt(){
		int maxVDPid = 3, lastSeqNr=28;
		String tableName = "tab5";
		
		MigrationStatus migrationStatus = new MigrationStatus(lastSeqNr, maxVDPid);
		try{
			System.out.println("Setting status ");
			ZKserver zKserver = new ZKserver(connectString);
			zKserver.setMigrationStatus(tableName, migrationStatus);
			MigrationStatus newStatus = zKserver.getMigrationStatus(tableName);

			assertEquals(migrationStatus.getLastSeqNr(), 
					newStatus.getLastSeqNr());
			System.out.println(newStatus.getLastSeqNr());
			if(migrationStatus!=null && newStatus!=null){
				HashMap<Integer, StateMachine> msVDPs = migrationStatus.getVDPs();
				HashMap<Integer, StateMachine> nsVDPs = newStatus.getVDPs();
				if(msVDPs!=null && nsVDPs!=null)
					assertEquals(msVDPs.size(), 
							nsVDPs.size());
				else
					assertFalse(true);
			}
			
			for(int i=0;i<maxVDPid;i++){
				HashMap<Integer, StateMachine> msVDPs = migrationStatus.getVDPs();
				HashMap<Integer, StateMachine> nsVDPs = newStatus.getVDPs();
				if(msVDPs!=null && nsVDPs!=null){
					System.out.println(msVDPs.size());
					assertEquals(msVDPs.get(i).getCurrentState().name(), 
							nsVDPs.get(i).getCurrentState().name());
					System.out.println("VDP"+i+": "+newStatus.getVDPs().get(i).getCurrentState().name());
				}else{
					assertFalse(true);
				}
			}
			
			newStatus.migrateVDP(maxVDPid-1);
			boolean setMigrationStatus = zKserver.setMigrationStatus(tableName, newStatus);
			assertTrue(setMigrationStatus);
			
			MigrationStatus newStatus2 = zKserver.getMigrationStatus(tableName);
			if(newStatus2!=null){
				assertEquals(""+VDPstatus.UNDER_MIGRATION.name(), 
					""+newStatus2.getVDPstatus(maxVDPid-1).getCurrentState().name());
				System.out.println("VDP"+(maxVDPid-1)+" new status is "+newStatus2.getVDPstatus(maxVDPid-1));
			}else{
				assertFalse(true);
			}
			zKserver.close();
			zKserver=null;
		} catch(Exception e){
			e.printStackTrace();
			fail("Threw an exception ");
		}
	}


	/**
	 * Test method for {@link it.polimi.hegira.zkWrapper.ZKserver#deleteMigrationStatuses()}.
	 * @throws Exception 
	 */
	@Ignore
	public void CtestDeleteMigrationStatuses() {
		
		int expectedSize = 0;
		try{
			System.out.println("Deleting all statuses!!!");
			ZKserver zKserver = new ZKserver(connectString);
			zKserver.deleteMigrationStatuses();
			List<String> migrationPaths = zKserver.getMigrationPaths();
			int size = migrationPaths.size();
			zKserver=null;
			assertEquals(expectedSize, size);
		} catch(Exception e){
			fail("Threw an exception");
		}
	}

	/**
	 * Test method for {@link it.polimi.hegira.zkWrapper.ZKserver#setVDPsize(int)}.
	 * @throws Exception 
	 */
	@Ignore
	public void DtestSetAndGetVDPsize() {
		int p = 3;
		try{
			ZKserver zKserver = new ZKserver(connectString);
			System.out.println("Setting VDPs size!!!");
			zKserver.setVDPsize(p);
			int retrievedVdpSize = zKserver.getVDPsize();
			System.out.println("Got back VDPs size = "+retrievedVdpSize);
			assertEquals(p, retrievedVdpSize);
		} catch(Exception e){
			fail("Threw an exception");
		}
	}
	
	
	@Ignore
	public void FtestQueryLock(){
		ZKserver zKserver = new ZKserver(connectString);
		try {
			zKserver.unlockQueries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		zKserver.close();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExecutorService service = Executors.newFixedThreadPool(1);
		service.submit(new Runnable() {
			
			public void run() {
				ZKserver zKserver = new ZKserver(connectString);
				SharedCountListener listener = new SharedCountListener() {
					
					public void stateChanged(CuratorFramework arg0, ConnectionState arg1) {
						System.out.println("State changed to: "+arg1.name());
						
					}
					
					public void countHasChanged(SharedCountReader sharedCount, int newCount)
							throws Exception {
						System.out.println("Value changed to: "+ZKclient.toBoolean(newCount));
					}
				};
				
				try {
					boolean locked = zKserver.isLocked(listener);
					System.out.println("Is Locked? "+locked);
					fail("");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		ZKserver zKserver2 = new ZKserver(connectString);
		try {
			zKserver2.lockQueries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		zKserver2.close();
	
	}
	
	@Ignore
	public void testFreshSetter(){
		ZKserver zKserverInit = new ZKserver(connectString);
		final String tblName = "testTable";
		final int no_VDPs = 3;
		int lastSeqNr = 4000;
		MigrationStatus migrationStatus = new MigrationStatus(lastSeqNr, no_VDPs);
		
		try {
			zKserverInit.setFreshMigrationStatus(tblName, migrationStatus);
			zKserverInit.close();
			zKserverInit = null;
			
			ExecutorService service = Executors.newFixedThreadPool(2);
			
			
			//Mocking the subscriber
			service.submit(new Runnable() {
				
				@Override
				public void run() {
					ZKserver zKserverSub = new ZKserver(connectString);
					try {
						Thread.sleep(300);
						int localVdpId = 2;
						System.out.println(Thread.currentThread().getName()+
								" SUBSCRIBER: Trying to synchronize on VDP "+localVdpId);
						MigrationStatus updatedStatus = zKserverSub.getFreshMigrationStatus(tblName, null);
						VDPstatus syncVDP = updatedStatus.syncVDP(localVdpId, SubscriberType.SOURCE);
						if(!syncVDP.equals(VDPstatus.NOT_MIGRATED) && !syncVDP.equals(VDPstatus.UNDER_MIGRATION)){
							bulletProofZKset(zKserverSub, tblName, updatedStatus, localVdpId);
							System.out.println(Thread.currentThread().getName()+
									" SUBSCRIBER: Propagating query. StateMachine was "+syncVDP.name());
							Thread.sleep(1000);
							
							updatedStatus = zKserverSub.getFreshMigrationStatus(tblName, null);
							updatedStatus.finish_syncVDP(localVdpId);
							bulletProofZKset(zKserverSub, tblName, updatedStatus, localVdpId);
							System.out.println(Thread.currentThread().getName()+
									" SUBSCRIBER: Finished Sync VDP "+localVdpId);
						}
						
					} catch (Exception e) {
						System.err.println(Thread.currentThread().getName()+
								" SUBSCRIBER: something wrong happened!!");
						fail(e.getMessage());
					} finally{
						zKserverSub.close();
						zKserverSub = null;
					}
					
				}
			});
			
			//Mocking up SRC
			service.submit(new Runnable() {
				
				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName()+
							" SRC: Trying to migrate everything ");
					final ZKserver zKserverSRC = new ZKserver(connectString);
					try {
						for(int i=0;i<=no_VDPs;i++){
							MigrationStatus mstatus;
							if(i==0){
								mstatus = zKserverSRC.getFreshMigrationStatus(tblName, new SharedValueListener() {
									
									@Override
									public void stateChanged(CuratorFramework arg0, ConnectionState arg1) {
										System.out.println(Thread.currentThread().getName()+
												" SRC: State changed to "+arg1);
									}
									
									@Override
									public void valueHasChanged(SharedValueReader arg0, byte[] arg1)
											throws Exception {
										MigrationStatus newms = (MigrationStatus) zKserverSRC.deserialize(arg1);
										
										System.out.println(Thread.currentThread().getName()+
												" SRC: VDPs changed to: ");
										Set<Integer> vdpsNo = newms.getVDPs().keySet();
										for(Integer no : vdpsNo){
											System.out.println("VDP "+no
													+": "+newms.getVDPstatus(no).getCurrentState());
										}
									}
								});
							}else{
								mstatus = zKserverSRC.getFreshMigrationStatus(tblName, null);
							}
							System.out.println(Thread.currentThread().getName()+
									" SRC: migrating VDP "+i+" whose current status is "+ 
										mstatus.getVDPstatus(i).getCurrentState());
							VDPstatus nextStatus = mstatus.migrateVDP(i);
							if(nextStatus.equals(VDPstatus.UNDER_MIGRATION)){
								bulletProofZKset(zKserverSRC, tblName, mstatus, i);
								Thread.sleep(1000);
							}else{
								System.out.println(Thread.currentThread().getName()+
										" SRC: Cannot migrate VDP "+i+". VDPstatus was "+nextStatus.name());
							}
							
						}
						
					} catch (Exception e) {
						fail(e.getMessage());
					} finally {
						zKserverSRC.close();
						//zKserverSRC = null;
					}
					
				}
			});
			
			service.awaitTermination(10, TimeUnit.SECONDS);
			
		} catch (Exception e1) {
			fail(e1.getMessage());
		}
	}
	
	private void bulletProofZKset(ZKserver zKserver, String tblName, MigrationStatus updatedStatus, int vdpid) throws Exception{
		int retries = 3;
		boolean done = true;
		do{
			if(retries<3){
				System.out.println(Thread.currentThread().getName()+" retring...");
				Thread.sleep(300);
			}
			System.out.println(Thread.currentThread().getName()+
					" updating ZooKeeper with "+updatedStatus.getVDPstatus(vdpid).getCurrentState()+
					" for vdp "+vdpid);
			done = zKserver.setFreshMigrationStatus(tblName, updatedStatus);
			retries--;
		}while(!done && retries>0);
	}
	
	@Ignore
	public void generateMigrationStatuses() throws Exception{
		ZKserver zKserverInit = new ZKserver(connectString);
		final String tblName = "prova";
		final int no_VDPs = 3;
		int lastSeqNr = 4000;
		MigrationStatus migrationStatus = new MigrationStatus(lastSeqNr, no_VDPs);
		zKserverInit.setFreshMigrationStatus(tblName, migrationStatus);
		zKserverInit.close();
		zKserverInit = null;
	}
	
	@Ignore
	public void testGetTableFreshVDPs(){
		ZKserver2 zk = new ZKserver2(connectString);
		final String tblName = "test1";
		HashMap<Integer, StateMachine> vdps = null;
		try {
			vdps = zk.getFreshVDPs(tblName);
			if(vdps==null){
				System.err.println("VDPs is null");
				throw new Exception("Got null VDPs");
			}
			for(Entry<Integer,StateMachine> e : vdps.entrySet()){
				System.out.println("VDP: "+e.getKey()+" state_ "+e.getValue().getCurrentState().name()+"\n");
			}
			assertTrue(vdps.size()>-1);
		} catch (Exception e) {
			System.err.println("Error getting fresh VDPs: \n"
					+e.getMessage()+"\n");
			e.printStackTrace();
			assertFalse(true);
		}
	}
	
	@Test
	public void testZkServer2() throws Exception{
		ZKserver2 zk = new ZKserver2(connectString);
		final String tblName = "test1";
		
		if(zk.acquireLock(tblName, "1"))
			System.out.println("Got lock "+tblName+"/"+"1");
		
		if(zk.acquireLock(tblName, "2"))
		System.out.println("Got lock "+tblName+"/"+"2");
		
		zk.releaseLock(tblName, "1");
		zk.releaseLock(tblName, "2");
	}
}
